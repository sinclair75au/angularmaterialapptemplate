
# Requirements

* [NodeJS](https://nodejs.org/download/)
* [Ruby](https://www.ruby-lang.org/en/documentation/installation/)
* [Grunt](http://gruntjs.com/installing-grunt)
* [Bower](http://bower.io/#install-bower)
* [Yoeman](http://yeoman.io/)

# Generator

[ng-super](https://github.com/mohuk/generator-ng-super)

# Starting

```
grunt server
```

# SASS

* Includes Google material
* Site design variables: app/styles/variables.scss