(function(){

  /* global module, inject */

  'use strict';

  describe('Controller: Happy', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.sports'));

    var ctrl;
    var scope;

    beforeEach(inject(function($controller, $injector){

      scope = $injector.get('$rootScope');

      ctrl = $controller('HappyCtrl', {
        //add injectable services
      });

    }));

    it('should do nothing', function(){
      expect(true).toBe(true);
    });

  });
}());
