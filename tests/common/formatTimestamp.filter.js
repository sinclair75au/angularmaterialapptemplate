(function(){

  /* global module, inject */

  'use strict';

  describe('Filter: formatTimestamp', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.common'));

    var formatTimestamp;

    beforeEach(inject(function (formatTimestampFilter){

      formatTimestamp = formatTimestampFilter;

    }));

    it('should not do anything for now', function(){
      expect(true).toBe(true)
     // expect(formatTimestamp(new Date())).toBe(true);
    });

  });
}());
