(function(){

  /* global module, inject */

  'use strict';

  describe('Filter: maxExpectationDisplay', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.common'));

    var maxExpectationDisplay;

    beforeEach(inject(function (maxExpectationDisplayFilter){

      maxExpectationDisplay = maxExpectationDisplayFilter;

    }));

    it('if input is number lower than 2.01 - should return the number', function(){
      expect( maxExpectationDisplay(2)).toBe(2);
    });

    it('if input is number bigger than 2.01 - should return "+2.01"', function(){
      expect( maxExpectationDisplay(2.02)).toBe("+2.01");
    });

    it('if input is NOT a  number  - should return unchanged input', function(){
      expect( maxExpectationDisplay('oo')).toBe("oo");
    });

  });
}());
