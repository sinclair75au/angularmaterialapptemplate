(function(){

  /* global module, inject */

  'use strict';

  describe('Filter: orderEvents', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.common'));

    var orderEvents;

    beforeEach(inject(function (orderEventsFilter){

      orderEvents = orderEventsFilter;

    }));

    it('should order the array by eventStartTime asc', function(){
      var event1={eventStartTime:2},
      event2={eventStartTime:1};//should go to first place after sorting
      var ordered  = orderEvents([event1, event2]);
      expect(ordered[0].eventStartTime).toBe(1);
    });

    it('if some item has "inPlay" property set to 1 - it should became first', function(){
      var event1={eventStartTime:2},
        event2={eventStartTime:1},
        event3={inPlay:1}//should go to first place after sorting

      var ordered  = orderEvents([event1, event2, event3]);
      expect(ordered[0].inPlay).toBe(1);
      expect(ordered[0].eventStartTime).toBe(0);
    });

  });
}());
