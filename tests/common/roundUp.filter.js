(function(){

  /* global module, inject */

  'use strict';

  describe('Filter: roundUp', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.common'));

    var roundUp;

    beforeEach(inject(function (roundUpFilter){

      roundUp = roundUpFilter;

    }));

    it('should not do anything for now', function(){
      expect(true).toBe(true);
    });

  });
}());
