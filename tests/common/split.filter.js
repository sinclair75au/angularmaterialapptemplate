(function(){

  /* global module, inject */

  'use strict';

  describe('Filter: split', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.common'));

    var split;

    beforeEach(inject(function (splitFilter){

      split = splitFilter;

    }));

    it('should split the input and show the item on index place', function(){
      expect(split('aaa,bbb',',',0)).toBe('aaa');
    });

    it('should split the input and show the item on index place', function(){
      expect(split('aaa,bbb',',',1)).toBe('bbb');
    });

    it('should not change the input if no index supplied', function(){
      expect(split('aaa,bbb',',')).toBe('aaa,bbb');
    });
  });
}());
