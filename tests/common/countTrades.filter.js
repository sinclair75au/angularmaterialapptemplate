(function(){

  /* global module, inject */

  'use strict';

  describe('Filter: countTrades', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.common'));

    var countTrades;

    beforeEach(inject(function (countTradesFilter){

      countTrades = countTradesFilter;

    }));

    it('if trades is NOT array  - should return "-"', function(){
      expect(countTrades(null)).toBe('-');
    });

    it('if trades is empty array  - should return 0', function(){
      expect(countTrades([])).toBe(0);
    });
    
    it('if trades is array & slipStatus is set  - should return  length of items with such "tradeStatus field"', function(){
      expect(countTrades([{tradeStatus:'something'},{tradeStatus:'somethingdifferent'}], 'something')).toBe(1);
    });

  });
}());
