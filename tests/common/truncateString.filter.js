(function(){

  /* global module, inject */

  'use strict';

  describe('Filter: truncateString', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.common'));

    var truncateString;

    beforeEach(inject(function (truncateStringFilter){

      truncateString = truncateStringFilter;

    }));

    it('should not do anything for now', function(){
      expect(truncateString('shalom',3)).toBe('sha..');
    });

  });
}());
