(function(){

  /* global module, inject */

  'use strict';

  describe('Filter: twentyFourHourWithSeconds', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.common'));

    var twentyFourHourWithSeconds;

    beforeEach(inject(function (twentyFourHourWithSecondsFilter){

      twentyFourHourWithSeconds = twentyFourHourWithSecondsFilter;

    }));

    it('should not do anything for now', function(){
      expect(moment.unix(1318781876).format('HH:mm:ss')).toBe("19:17:56");
    });

  });
}());
