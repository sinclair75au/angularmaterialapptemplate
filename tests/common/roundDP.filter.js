(function(){

  /* global module, inject */

  'use strict';

  describe('Filter: roundDP', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.common'));

    var roundDP;

    beforeEach(inject(function (roundDPFilter){

      roundDP = roundDPFilter;

    }));

    it('should round number to 2 digits if decimalPlaces is 2', function(){
      expect(roundDP(12.690, 2)).toBe('12.69');
    });

  });
}());
