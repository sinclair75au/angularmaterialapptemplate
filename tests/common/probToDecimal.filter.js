(function(){

  /* global module, inject */

  'use strict';

  describe('Filter: probToDecimal', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.common'));

    var probToDecimal;

    beforeEach(inject(function (probToDecimalFilter){

      probToDecimal = probToDecimalFilter;

    }));

    it('should add the input to "probToDecimal" string', function(){
      expect(probToDecimal('2')).toBe('probToDecimal filter: 2');
    });

  });
}());
