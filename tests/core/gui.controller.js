(function(){

  /* global module, inject */

  'use strict';

  describe('Controller: Gui', function(){

    beforeEach(module('app.core'));
    beforeEach(module('app.core'));

    var ctrl;
    var scope;

    beforeEach(inject(function($controller, $injector){

      scope = $injector.get('$rootScope');

      ctrl = $controller('Gui', {
        //add injectable services
      });

    }));

    it('should do nothing', function(){
      expect(true).toBe(true);
    });

  });
}());
