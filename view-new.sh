#!/bin/bash
if [ "$#" -ne 1 ]
then
    echo "Missing parameter modulename.viewname"
else
    yo ng-super:view $1
    yo ng-super:controller $1
fi
