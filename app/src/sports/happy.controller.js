/**
 * @ngdoc controller
 * @name app.sports.controller:Happy
 * @description < description placeholder >
 */

(function(){

  'use strict';

	angular
		.module('app.sports')
		.controller('HappyCtrl', Happy);

  /* @ngInject */
	function Happy(){
		var vm = this;

		vm.testFunction = testFunction;

    /////////////////////

    /**
     * @ngdoc method
     * @name testFunction
     * @param {number} num number is the number of the number
     * @methodOf app.sports.controller:Happy
     * @description
     * My Description rules
     */
    function testFunction(num){
			console.info('This is a test function');
		}
	}

}());
