/**
 * @ngdoc overview
 * @name app.sports
 * @description < description placeholder >
 */

(function(){

  'use strict';

  angular
    .module('app.sports', [])
    .config(configuration);

  /* @ngInject */
  function configuration($stateProvider){

    //add your state mappings here
    $stateProvider
      .state('SportsHome', {
        url:'/sports',
        templateUrl:'src/sports/home.html',
        controller: 'SportHomeCtrl as vm'
      }
    );
    
    $stateProvider
      .state('SportsHappy', {
        url:'/happy',
        templateUrl:'src/sports/happy.html',
        controller: 'HappyCtrl as vm'
      }
    );
  }

}());
