/**
 * @ngdoc filter
 * @name app.common.filer:twentyFourHourWithSeconds
 * @description Convert to 25 hour clock
 * @param {String} Date time string
 * @returns {String} 24 hours clock time
 */

(function(){

  'use strict';

  angular
    .module('app.common')
    .filter('twentyFourHourWithSeconds', twentyFourHourWithSeconds);

  /* @ngInject */
  function twentyFourHourWithSeconds(){
    return function(dateTimeString) {
      var eventDateTime = moment.unix(dateTimeString);

      return eventDateTime.format('HH:mm:ss');
    };
  }

}());
