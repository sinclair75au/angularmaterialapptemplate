/**
 * @ngdoc filter
 * @name app.common.filer:split
 * @description Angular filter that splits a string and returns the nth item
 * @param {String} input object to be filtered
 * @param {Char} split char
 * @param {Number} index of item to be returned
 * @returns {String} nth item e.g. ("1,2,3", ",", 0) would return "1"
 */

(function(){

  'use strict';

  angular
    .module('app.common')
    .filter('split', split);

  /* @ngInject */
  function split(){
    return function(input, splitChar, splitIndex) {
      if (input === undefined || !input || !input.length) { return; }
      if(splitIndex=== undefined ){ return input;}
      return input.split(splitChar)[splitIndex];
    };
  }

}());
