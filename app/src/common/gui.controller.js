/**
 * @ngdoc controller
 * @name app.common.controller:GuiCommonCtrl
 * @description < description placeholder >
 */

(function(){

  'use strict';

	angular
		.module('app.common')
		.controller('GuiCommonCtrl', GuiCommonCtrl, ['$scope','$mdSidenav']);

  /* @ngInject */
	function GuiCommonCtrl($scope,$mdSidenav){
		var vm = $scope;

		vm.testFunction = testFunction;
    vm.toggleMenu = toggleMenu;
    vm.menuToggleStatus = menuToggleStatus;
    vm.siteConfig = {
      'name': 'Sports portal',
      'menu': [
        {
        'name': 'Item 1',
        'type': 'heading',
        'open': false,
        'pages': [{
                  'name': 'Item 1.1',
                  'icon': '',
                  'url': '/CSS/typography',
                  'stateName': 'SportsHome',
                  'type': 'link'
                },
                {
                  'name' : 'Item 1.2',
                  'url': '/CSS/button',
                  'stateName': 'SportsHappy',
                  'type': 'link'
                }]
        }
      ]
    };

    /////////////////////

    /**
     * @ngdoc method
     * @name testFunction
     * @param {number} num number is the number of the number
     * @methodOf app.common.controller:Gui
     * @description
     * My Description rules
     */
    function testFunction(num){
			console.info('This is a test function');
		}
    
    /**
     * 
     */
    function toggleMenu(pos, keepOpen){
      var nav = $mdSidenav(pos);
      var open = nav.isOpen();
      if(keepOpen) {
        nav.open();
      } else {
        nav.toggle();
      }
		}
    
    /**
     * 
     */
     function menuToggleStatus(pos){
      var nav = $mdSidenav(pos);
      return nav.isOpen();
		}
	}

}());
