/**
 * @ngdoc filter
 * @name app.common.filer:formatTimestamp
 * @description < description placeholder >
 * @param {object} input object to be filtered
 * @returns {object} < returns placeholder >
 */

(function(){

  'use strict';

  angular
    .module('app.common')
    .filter('formatTimestamp', [ '$sce', formatTimestamp]);

  /* @ngInject */
  function formatTimestamp( $sce ){
    return function(dateTimeString) {
      var eventDateTime = moment.unix(dateTimeString);
      var todayDateTime = moment();
      var formattedTime = eventDateTime.format('HH:mm');

      if (todayDateTime.format('dddd') !== eventDateTime.format('dddd')) {
        formattedTime += ' <sup>(+1)</sup>';
      }

      return $sce.trustAsHtml(formattedTime);
    };
  }

}());
