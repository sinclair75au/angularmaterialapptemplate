/**
 * @ngdoc filter
 * @name app.common.filer:probToDecimal
 * @description < description placeholder >
 * @param {object} input object to be filtered
 * @returns {object} < returns placeholder >
 */

(function(){

  'use strict';

  angular
    .module('app.common')
    .filter('probToDecimal', probToDecimal);

  /* @ngInject */
  function probToDecimal(){
    return function (input){
      return 'probToDecimal filter: ' + input;
    };
  }

}());
