/**
 * @ngdoc filter
 * @name app.common.filer:maxExpectationDisplay
 * @description Ensure input does not exceed 2.01
 * @param {Number} input object to be filtered
 * @returns {String/Number} Value no greater that +2.01
 */

(function(){

  'use strict';

  angular
    .module('app.common')
    .filter('maxExpectationDisplay', maxExpectationDisplay);

  /* @ngInject */
  function maxExpectationDisplay(){
    return function(input) {
      if(isNaN(input)){
        return input;
      }
      else if (input > 2.01) {
        return '+2.01';
      }
      return input;
    };
  }

}());
