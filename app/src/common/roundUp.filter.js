/**
 * @ngdoc filter
 * @name app.common.filer:roundUp
 * @description Round a number to the highest decimal place
 * @param {number} number to be rounded up
 * @returns {number} number rounded up e.g. (0.1) would return 1
 */

(function(){

  'use strict';

  angular
    .module('app.common')
    .filter('roundUp', roundUp);

  /* @ngInject */
  function roundUp(){
    return function(input) {
      if (input === undefined || !input) { return input; }
      return Math.round(parseInt(input, 10));
    };
  }

}());
