/**
 * @ngdoc filter
 * @name app.common.filer:roundDP
 * @description < description placeholder >
 * @param {Number} input object to be filtered
 * @param {Number} number of places to round to
 * @returns {Number} rounded number
 */

(function(){

  'use strict';

  angular
    .module('app.common')
    .filter('roundDP', roundDP);

  /* @ngInject */
  function roundDP(){
    return function(input, decimalPlaces) {
      if (input === undefined || !input) { return input; }
      return parseFloat(input).toFixed(decimalPlaces);
    };
  }

}());
