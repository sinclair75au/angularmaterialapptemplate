/**
 * @ngdoc filter
 * @name app.common.filer:orderEvents
 * @description Sort event by eventStartTime
 * @param {Array} input object to be filtered
 * @returns {Array} sorted array
 */

(function(){

  'use strict';

  angular
    .module('app.common')
    .filter('orderEvents', orderEvents);

  /* @ngInject */
  function orderEvents(){
    return function(events) {
      var filtered = [];
      angular.forEach(events, function(evt) {
        if (evt.inPlay === 1) {
          evt.eventStartTime = 0;
        }
        filtered.push(evt);
      });
      filtered.sort(function (a, b) {
        a = parseInt(a.eventStartTime);
        b = parseInt(b.eventStartTime);

        return a - b;
      });
      return filtered;
    };
  }

}());
