/**
 * @ngdoc filter
 * @name app.common.filer:countTrades
 * @description Returns the number of trades or '-'
 * @param {Array} input object to be filtered
 * @returns {Number/String} Returns 0 or greater or '-'
 */

(function(){

  'use strict';

  angular
    .module('app.common')
    .filter('countTrades', countTrades);

  /* @ngInject */
  function countTrades(){
    return function(trades, slipStatus) {
    var count = 0;
      if (!trades || !angular.isArray(trades) ) {
        return '-';
      }

      angular.forEach(trades, function(trade) {
        if (slipStatus === '' || trade.tradeStatus === slipStatus) {
          count+=1;
        }
      });
      return count;
    };
  }

}());
