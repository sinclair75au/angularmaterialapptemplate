/**
 * @ngdoc filter
 * @name app.common.filer:truncateString
 * @description Truncatea string n charachers
 * @param {String} input object to be filtered
 * @param {Number} maximum length of string to return
 * @returns {String} truncated strng
 */

(function(){

  'use strict';

  angular
    .module('app.common')
    .filter('truncateString', truncateString);

  /* @ngInject */
  function truncateString(){
    return function(input, maxLength) {
      if (input === undefined || !input || !input.length) { return input; }
      if (input.length > maxLength) {
        return input.substring(0, maxLength) + '..';
      } else {
        return input;
      }
  };
  }

}());
