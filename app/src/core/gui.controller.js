/**
 * @ngdoc controller
 * @name app.core.controller:Gui
 * @description < description placeholder >
 */

(function(){

  'use strict';

	angular
		.module('app.core')
		.controller('Gui', Gui, ['$mdSidenav']);

  /* @ngInject */
	function Gui($mdSidenav){
		var vm = this;

		vm.testFunction = testFunction;
    
    vm.openLeftMenu = function() {
      debugger;
      $mdSidenav('left').toggle();
    };

    /////////////////////

    /**
     * @ngdoc method
     * @name testFunction
     * @param {number} num number is the number of the number
     * @methodOf app.core.controller:Gui
     * @description
     * My Description rules
     */
    function testFunction(num){
			console.info('This is a test function');
		}
	}

}());
